﻿using UnityEngine;
using System.Collections;
using System.Text;
using System;
using UnityEngine.UI;

public class MainEventMain : MonoBehaviour {
    AndroidJavaClass ajc;
    int boothNum = 21;
    string readDataNFC;
    const int maxByteNFC = 36;
    public Text printerNameTx;
    // Use this for initialization
    void Start () {
         //int intAry = Convert.ToInt32("01" + "0F", 16);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void writeNFC()
    {
        CommonFn.callLoading(true);
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(ShareProfile.pathJavaObj);
            byte[] bAry = new byte[maxByteNFC];
            #region init write test
            //48000001
            bAry[0] = 0x04;
            bAry[1] = 0x08;
            bAry[2] = 0x00;
            bAry[3] = 0x00;
            bAry[4] = 0x00;
            bAry[5] = 0x00;
            bAry[6] = 0x00;
            bAry[7] = 0x01;
            // time stamp start 
            string dd = DateTime.Now.Day.ToString();
            string mm = DateTime.Now.Month.ToString();
            string yy = DateTime.Now.Year.ToString().Substring(2, 2);
            string hh = DateTime.Now.Hour.ToString();
            string min = DateTime.Now.Minute.ToString();
            string sec = DateTime.Now.Second.ToString();
            bAry[8] = Convert.ToByte(dd);
            bAry[9] = Convert.ToByte(mm);
            bAry[10] = Convert.ToByte(yy);
            bAry[11] = Convert.ToByte(hh);
            bAry[12] = Convert.ToByte(min);
            bAry[13] = Convert.ToByte(sec);
            // time stamp end
            bAry[14] = 0x0A;
            bAry[15] = 0x0B;
            bAry[16] = 0x0C;
            bAry[17] = 0x0D;
            bAry[18] = 0x0E;
            bAry[19] = 0x0F;
            //// data
            bAry[20] = 0xD4; 
            bAry[21] = 0x31;// 54321
            bAry[22] = 0x00;
            bAry[23] = 0x00;

            bAry[24] = 0x00;
            bAry[25] = 0x00;
            bAry[26] = 0x00;
            bAry[27] = 0x00;

            bAry[28] = 0x00;
            bAry[29] = 0x00;
            bAry[30] = 0x00;
            bAry[31] = 0x00;

            bAry[32] = 0x00;
            bAry[33] = 0x00;
            bAry[34] = 0xFA;
            bAry[35] = 0xFB;
            // this 36 byte = 43 byte in NFC tool
            #endregion

            int vals = ajc.CallStatic<int>("writeTagNDEF", bAry);
            //int vals = ajc.CallStatic<int>("writeTagNDEF", dummyData);
            //int vals = ajc.CallStatic<int>("writeTagClick", 0, 7, dummyData, passDefault);
            //printerNameTx.text = "success Write";
            Debug.Log("write Tag Process = " + vals);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            CommonFn.callLoading(false);
        }
        CommonFn.callLoading(false);
    }
    public void writeNFCBooth()
    {
        int pointBooth = 1;
        string result = "";
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(ShareProfile.pathJavaObj);
            string[] valSplit = readDataNFC.Split('_');
            int[] intAry = new int[maxByteNFC];
            for (int i = 0; i < maxByteNFC; i++)
            {
                intAry[i] = Convert.ToInt32(valSplit[i], 16);
                result += " " + intAry[i].ToString();
            }
            Debug.Log("data before write = " + result);
            int indexDataWrite = (boothNum-1) / 5;
            Debug.Log("indexDataWrite = " + indexDataWrite);
            Debug.Log("valSplit[(indexDataWrite * 2) + 20]=" + valSplit[(indexDataWrite * 2) + 20]);
            Debug.Log("valSplit[(indexDataWrite * 2) + 21]=" + valSplit[(indexDataWrite * 2) + 21]);
            int data=Convert.ToInt32(valSplit[(indexDataWrite * 2) + 20] + valSplit[(indexDataWrite * 2) + 21], 16);
            Debug.Log("data before write=" + data);
            StringBuilder sb = new StringBuilder(data.ToString("D5"));
            int indexFive = (boothNum-1) % 5;
            sb[indexFive] = Convert.ToChar(pointBooth.ToString());
            string writeStr = sb.ToString();
            int writeInt = Convert.ToInt32(writeStr);
            Debug.Log("data after write=" + writeInt);
            intAry[(indexDataWrite * 2) + 20] = (byte)((writeInt >> 8) & 0xFF);
            intAry[(indexDataWrite * 2) + 21] = (byte)((writeInt) & 0xFF);
            byte[] writeByte = new byte[intAry.Length];
            for(int i =0;i<intAry.Length;i++)
            {
                writeByte[i] = (byte)intAry[i];
            }
            string hex = BitConverter.ToString(writeByte);
            Debug.Log("hex=" + hex);
            int resultWrite = ajc.CallStatic<int>("writeTagNDEF", writeByte);
            if(resultWrite==1)
            {
                printerNameTx.text = "success Write";
                Debug.Log("success Write");
            }
            else
            {
                Debug.Log("fail Write");
                printerNameTx.text = "fail Write";
            }
            //int vals = ajc.CallStatic<int>("writeTagNDEF", dummyData);
            //int vals = ajc.CallStatic<int>("writeTagClick", 0, 7, dummyData, passDefault);
            
        }
        catch(Exception e)
        {
            Debug.LogWarning(e);
        }
    }
    #region event from java
    public void GetUID(string id)
    {
        Debug.Log("GetUID = " + id);
    }
    public void ResultReadNdef(string val)
    {
        Debug.Log("ResultReadNdef  = " + val);
        readDataNFC = val;
        string result = "";
        try
        {
            string[] valSplit = val.Split('_');
            int[] intAry = new int[valSplit.Length];
            //Debug.Log("len = " + valSplit.Length);
            for (int i=0;i< 19;i++)
            {
                intAry[i] = Convert.ToInt32(valSplit[i],16);
                result += " " + intAry[i].ToString();
            }
            result += " | ";
            int[] dataNFC = new int[8];
            for (int i = 0; i < 8; i++)
            {
                intAry[i+20] = Convert.ToInt32(valSplit[(i*2)+20]+ valSplit[(i * 2)+21], 16);
                dataNFC[i] = Convert.ToInt32(valSplit[(i * 2) + 20] + valSplit[(i * 2) + 21], 16);
                result += " " + intAry[i + 20].ToString("D5");
            }
            //string temp = dataNFC[0].ToString("D5");
            //Debug.Log("dataNFC=" + temp);
            //StringBuilder sb = new StringBuilder(temp);
            //sb[2] = '1';
            //string writeStr = sb.ToString();
            //int writeInt = Convert.ToInt32(writeStr);
            //Debug.Log("writeInt=" + writeInt);
            //byte[] testB = new byte[2];
            //testB[0] = (byte)((writeInt >> 8)&0xFF);
            //testB[1] = (byte)((writeInt) & 0xFF);
            ////for (int i =0;i< testB.Length; i++)
            //{
            //    string hex = BitConverter.ToString(testB);
            //    Debug.Log("hex=" + hex);
            //}
            Debug.Log("convert to int all data  " + result);
        }
        catch(Exception e)
        {
            Debug.LogWarning(e);
        }
    }
    #endregion
}
