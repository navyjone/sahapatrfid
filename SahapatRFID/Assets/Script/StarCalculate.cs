﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StarCalculate : MonoBehaviour {

    public GameObject star1;
    public GameObject star2;
    public GameObject star3;
    public GameObject star4;
    public GameObject star5;

    public Color32 onText;
    public Color32 offText;
    public Color32 offColor;
    public Color32 onColor;

    public Text point;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void starClick (int starNo)
    {
        point.text = starNo.ToString();
        switch (starNo)
        {
            case 0:
                {
                    StarOff(star5);
                    StarOff(star4);
                    StarOff(star3);
                    StarOff(star2);
                    StarOff(star1);
                    break;
                }
            case 1:
                {
                    StarOff(star5);
                    StarOff(star4);
                    StarOff(star3);
                    StarOff(star2);
                    StarOn(star1);
                    break;
                }
            case 2:
                {
                    StarOff(star5);
                    StarOff(star4);
                    StarOff(star3);
                    StarOn(star2);
                    StarOn(star1);
                    break;
                }
            case 3:
                {
                    StarOff(star5);
                    StarOff(star4);
                    StarOn(star3);
                    StarOn(star2);
                    StarOn(star1);
                    break;
                }
            case 4:
                {
                    StarOff(star5);
                    StarOn(star4);
                    StarOn(star3);
                    StarOn(star2);
                    StarOn(star1);
                    break;
                }
            case 5:
                {
                    StarOn(star5);
                    StarOn(star4);
                    StarOn(star3);
                    StarOn(star2);
                    StarOn(star1);
                    break;
                }
        }
            
    }
    void OnEnable()
    {
        starClick(0);
    }
    void StarOff (GameObject target)
    {
        target.GetComponent<Image>().color = offColor;
        target.GetComponentInChildren<Text>().color = offText;
    }

    void StarOn(GameObject target)
    {
        target.GetComponent<Image>().color = onColor;
        target.GetComponentInChildren<Text>().color = onText;
    }

    public void SubmitPush ()
    {
        //this.gameObject.SetActive(false);
    }
}
