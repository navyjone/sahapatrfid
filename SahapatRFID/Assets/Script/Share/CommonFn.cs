﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class CommonFn : MonoBehaviour {

	void Start () 
	{
	}
	void Update () 
	{	
		if(ShareProfile.isLoading)
		{
			ShareProfile.isLoading = false;
			if (ShareProfile.isCallLoading) 
			{
				GameObject uiInstance = (GameObject)GameObject.Instantiate (Resources.Load<GameObject> ("Prefabs/LoadingCanvas"));
				uiInstance.name = "LoadingCanvas";
				uiInstance.SetActive (true);
			} 
			else 
			{
				try
				{
					GameObject loading = GameObject.Find("LoadingCanvas");
					Destroy(loading);
				}
				catch
				{
				}
			}
		}
	}
	public static void callLoading(bool start)
	{
		if(start) 
		{
			ShareProfile.isLoading=true;
			ShareProfile.isCallLoading=true;
		} 
		else 
		{
			ShareProfile.isLoading=true;
			ShareProfile.isCallLoading=false;
		}
	}
	public void PageLink (string pageName)
	{
		Application.LoadLevel(pageName);
	}
	public static void SpawnMessage (string header, string body, string button, Action action1)
	{
		GameObject instant = Resources.Load("Prefabs/Message") as GameObject;
		GameObject messagePopup = GameObject.Instantiate(instant) as GameObject;
		messagePopup.name = "Message";

		Message.headerText = header;
		Message.bodyText = body;
		Message.button1Text = button;
		Message.button1Action = action1;
	}
	public static void SpawnMessageSelect (string header, string body, string button1, string button2, Action action1, Action action2)
	{
		GameObject instant = Resources.Load("Prefabs/MessageSelect") as GameObject;
		GameObject messagePopup = GameObject.Instantiate(instant) as GameObject;
		messagePopup.name = "MessageSelect";
		
		Message.headerText = header;
		Message.bodyText = body;
		Message.button1Text = button1;
		Message.button2Text = button2;
		Message.button1Action = action1;
		Message.button2Action = action2;
	}
	public void LogOutBtClick()
	{
		SpawnMessageSelect("Log Out","Do you want to log out?","Yes","No",()=>{ BackToLogin();},()=>{});
	}
	void BackToLogin()
	{
		PlayerPrefs.DeleteAll();
		Application.LoadLevel("Login");
	}
	public void LanguageClick (string language)
	{
		if (language == "Thai")
		{

		} 
		else if (language == "English")
		{

		}
	}
}

public static class TransformEx
{
    public static Transform Clear(this Transform transform)
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        return transform;
    }
}