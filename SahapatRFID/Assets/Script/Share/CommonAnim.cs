﻿using UnityEngine;
using System.Collections;

public class CommonAnim : MonoBehaviour {


 	public void ParameterTrue (string param)
	{
		this.gameObject.GetComponent<Animator>().SetBool(param,true);
	}

	public void ParameterFalse (string param)
	{
		this.gameObject.GetComponent<Animator>().SetBool(param,false);
	}

	public void ParticalPlay ()
	{
		this.gameObject.GetComponent<ParticleSystem>().Play();
	}

	public void ParticalStop ()
	{
		this.gameObject.GetComponent<ParticleSystem>().Stop();
	}


}
