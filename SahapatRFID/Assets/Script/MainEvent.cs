﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using UnityEngine.UI;

public class MainEvent : MonoBehaviour {

    public GameObject sidePanel;
    public GameObject sidePanelBack;
    public Text bodyTx;
    public Text pointTx;
    public GameObject contentScrollView;
    public InputField boothNOInput;
    public GameObject writeDonePopUp;
    public GameObject ScorePopup;
    public GameObject AuthenBoothPanel;
    public float moveRatio;
    public InputField passwordInput;
    public InputField newBoothNOInput;
    public Text headerBoothScoreTx;
    public Text boothFontTx;
    AndroidJavaClass ajc;
    int boothNOInt = 0;
    string readDataNFC;
    const int maxByteNFC = 40;
    private float margin = 1.0f;
    private Vector3 inPos;
    private Vector3 outPos;
    private bool moveOut = false;
    private bool moveIn = false;
    //toast
    string toastString;
    string input;
    ArrayList boothList;
    AndroidJavaObject currentActivity;
    AndroidJavaClass UnityPlayer;
    AndroidJavaObject context;
    // Use this for initialization
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        }
        if (PlayerPrefs.HasKey("boothNO"))
        {
            boothNOInt = PlayerPrefs.GetInt("boothNO");
            boothNOInput.text = boothNOInt.ToString();
            boothFontTx.text = "Booth No " + boothNOInt.ToString();
        }
        //width management
        float panelWidth = sidePanel.GetComponent<RectTransform>().rect.width;
        //sidePanel.GetComponent<RectTransform>().offsetMin = new Vector2(-panelWidth, 0f);
        float yPos = sidePanel.GetComponent<RectTransform>().anchoredPosition3D.y;
        float zPos = sidePanel.GetComponent<RectTransform>().anchoredPosition3D.z;
        inPos = new Vector3(0f, yPos, zPos);
        outPos =  sidePanel.GetComponent<RectTransform>().anchoredPosition3D;
        //sidePanel.GetComponent<RectTransform>().anchoredPosition3D = outPos;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (moveIn)
        {
            Vector3 nowPos = sidePanel.GetComponent<RectTransform>().anchoredPosition3D;
            sidePanel.GetComponent<RectTransform>().anchoredPosition3D = Vector3.Lerp(nowPos, inPos, moveRatio);
            moveOut = false;
        }
        if (sidePanel.GetComponent<RectTransform>().anchoredPosition3D.x >= inPos.x - margin)
        {
            sidePanel.GetComponent<RectTransform>().anchoredPosition3D = inPos;
            moveIn = false;
        }

        if (moveOut)
        {
            Vector3 nowPos = sidePanel.GetComponent<RectTransform>().anchoredPosition3D;
            sidePanel.GetComponent<RectTransform>().anchoredPosition3D = Vector3.Lerp(nowPos, outPos, moveRatio);
            moveIn = false;
        }
        if (moveOut && sidePanel.GetComponent<RectTransform>().anchoredPosition3D.x <= outPos.x + margin)
        {
            sidePanel.GetComponent<RectTransform>().anchoredPosition3D = outPos;
            moveOut = false;
        }

        if (!moveIn && !moveOut)
        {
            if (sidePanel.GetComponent<RectTransform>().anchoredPosition3D.x >= -100)
            {
                sidePanel.GetComponent<RectTransform>().anchoredPosition3D = inPos;

            }
            else
            {
                sidePanel.GetComponent<RectTransform>().anchoredPosition3D = outPos;
            }
        }
    }

    public void MoveOut()
    {
        moveOut = true;
        sidePanelBack.SetActive(false);
    }

    public void MoveIn()
    {
        moveIn = true;
        sidePanelBack.SetActive(true);
    }

    public void SubmitChangeBoothBtClick()
    {
        if(passwordInput.text != "2940294")
        {
            toastString = "Wrong password";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
            AuthenBoothPanel.SetActive(false);
            return;
        }
        try
        {
            if (newBoothNOInput.text == "")
            {
                toastString = "new booth is not right format";
                currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
                AuthenBoothPanel.SetActive(false);
            }
            boothNOInput.text = newBoothNOInput.text;
            boothFontTx.text = "Booth No "+newBoothNOInput.text;
            boothNOInt = Convert.ToInt32(boothNOInput.text);
            PlayerPrefs.SetInt("boothNO", boothNOInt);
            bodyTx.text = "Change BoothNO to = " + boothNOInt.ToString();
            toastString = "Change booth number complete";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
            AuthenBoothPanel.SetActive(false);
        }
        catch (Exception e)
        {
            toastString = "new booth is not right format";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
            AuthenBoothPanel.SetActive(false);
            bodyTx.text = e.ToString();
        }
    }
    public void SubmitStarBtClick()
    {
        int point = Convert.ToInt32(pointTx.text);
        if ((pointTx.text!="")&&(point!=0))
        {
            WriteNFCBooth(point);
        }
        else if(point == 0)
        {
            Debug.Log("point is zero");
            toastString = "point is zero";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
        }
        else if(point > 5)
        {
            Debug.Log("point is more than 5");
            toastString = "point is more than 5";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
        }
        else
        {
            Debug.Log("point is null");
        }
    }
    void showToast()
    {
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
        toast.Call("show");
    }
    #region write booth
    public void WriteNFC()
    {
        CommonFn.callLoading(true);
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(ShareProfile.pathJavaObj);
            byte[] bAry = new byte[maxByteNFC];
            #region init write test
            //48000001
            bAry[0] = 0x04;
            bAry[1] = 0x08;
            bAry[2] = 0x00;
            bAry[3] = 0x00;
            bAry[4] = 0x00;
            bAry[5] = 0x00;
            bAry[6] = 0x00;
            bAry[7] = 0x01;
            // time stamp start 
            string dd = DateTime.Now.Day.ToString();
            string mm = DateTime.Now.Month.ToString();
            string yy = DateTime.Now.Year.ToString().Substring(2, 2);
            string hh = DateTime.Now.Hour.ToString();
            string min = DateTime.Now.Minute.ToString();
            string sec = DateTime.Now.Second.ToString();
            bAry[8] = Convert.ToByte(dd);
            bAry[9] = Convert.ToByte(mm);
            bAry[10] = Convert.ToByte(yy);
            bAry[11] = Convert.ToByte(hh);
            bAry[12] = Convert.ToByte(min);
            bAry[13] = Convert.ToByte(sec);
            // time stamp end
            bAry[14] = 0x00;
            bAry[15] = 0x00;
            bAry[16] = 0x00;
            bAry[17] = 0x00;
            bAry[18] = 0x00;
            bAry[19] = 0x00;
            //// data
            bAry[20] = 0x00;// 54321
            bAry[21] = 0x00;
            bAry[22] = 0x00;
            bAry[23] = 0x00;

            bAry[24] = 0x00;
            bAry[25] = 0x00;
            bAry[26] = 0x00;
            bAry[27] = 0x00;

            bAry[28] = 0x00;
            bAry[29] = 0x00;
            bAry[30] = 0x00;
            bAry[31] = 0x00;

            bAry[32] = 0x00;
            bAry[33] = 0x00;
            bAry[34] = 0x00;
            bAry[35] = 0x00;
            // this 36 byte = 43 byte in NFC tool
            bAry[36] = 0x00;
            bAry[37] = 0x00;
            bAry[38] = 0x00;
            bAry[39] = 0x21;
            // this 40 byte = 50 byte in NFC tool
            #endregion

            int vals = ajc.CallStatic<int>("writeTagNDEF", bAry);
            //int vals = ajc.CallStatic<int>("writeTagNDEF", dummyData);
            //int vals = ajc.CallStatic<int>("writeTagClick", 0, 7, dummyData, passDefault);
            //printerNameTx.text = "success Write";
            Debug.Log("write Tag Process = " + vals);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            CommonFn.callLoading(false);
        }
        CommonFn.callLoading(false);
    }
    public void WriteNFCBooth(int pointBooth)
    {
        //int pointBooth = 1;
        string result = "";
        try
        {
            AndroidJavaClass ajc = new AndroidJavaClass(ShareProfile.pathJavaObj);
            string[] valSplit = readDataNFC.Split('_');
            int[] intAry = new int[maxByteNFC];
            for (int i = 0; i < maxByteNFC; i++)
            {
                intAry[i] = Convert.ToInt32(valSplit[i], 16);
                result += " " + intAry[i].ToString();
            }
            Debug.Log("data before write = " + result);
            int indexDataWrite = (boothNOInt - 1) / 5;
            //Debug.Log("indexDataWrite = " + indexDataWrite);
            //Debug.Log("valSplit[(indexDataWrite * 2) + 20]=" + valSplit[(indexDataWrite * 2) + 20]);
            //Debug.Log("valSplit[(indexDataWrite * 2) + 21]=" + valSplit[(indexDataWrite * 2) + 21]);
            int data = Convert.ToInt32(valSplit[(indexDataWrite * 2) + 20] + valSplit[(indexDataWrite * 2) + 21], 16);
            Debug.Log("data before write=" + data);
            StringBuilder sb = new StringBuilder(data.ToString("D5"));
            int indexFive = (boothNOInt - 1) % 5;
            sb[indexFive] = Convert.ToChar(pointBooth.ToString());
            string writeStr = sb.ToString();
            int writeInt = Convert.ToInt32(writeStr);
            Debug.Log("data after write=" + writeInt);
            intAry[(indexDataWrite * 2) + 20] = (byte)((writeInt >> 8) & 0xFF);
            intAry[(indexDataWrite * 2) + 21] = (byte)((writeInt) & 0xFF);
            byte[] writeByte = new byte[intAry.Length];
            for (int i = 0; i < intAry.Length; i++)
            {
                writeByte[i] = (byte)intAry[i];
            }
            string hex = BitConverter.ToString(writeByte);
            Debug.Log("hex=" + hex);
            int resultWrite = ajc.CallStatic<int>("writeTagNDEF", writeByte);
            if (resultWrite == 1)
            {
                bodyTx.text = "Write Success";
                writeDonePopUp.SetActive(true);
                ScorePopup.SetActive(false);
                Debug.Log("Write Success");
                toastString = "Write Success";
                currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
            }
            else
            {
                Debug.Log("Write Fail");
                bodyTx.text = "Write Fail";
                toastString = "Write Fail";
                currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
            }
            //int vals = ajc.CallStatic<int>("writeTagNDEF", dummyData);
            //int vals = ajc.CallStatic<int>("writeTagClick", 0, 7, dummyData, passDefault);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            toastString = "Write Card Error";
            currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
        }
    }
    #endregion
    #region event from java
    public void GetUID(string id)
    {
        Debug.Log("GetUID = " + id);
    }
    public void ResultReadNdef(string val)
    {
        Debug.Log("ResultReadNdef  = " + val);
        readDataNFC = val.Split(',')[0];
        string result = "";
        boothList = new ArrayList();
        try
        {
            string[] valSplit = val.Split(',')[0].Split('_');
            //string[] valSplit = val.Split('_');
            int[] intAry = new int[valSplit.Length];
            Debug.Log("len = " + valSplit.Length);
            for (int i = 0; i < 20; i++)
            {
                intAry[i] = Convert.ToInt32(valSplit[i], 16);
                result += " " + intAry[i].ToString();
            }
            result += " | ";
            int lenData = 10;
            int[] dataNFC = new int[lenData];
            for (int i = 0; i < lenData; i++)
            {
                intAry[i + 20] = Convert.ToInt32(valSplit[(i * 2) + 20] + valSplit[(i * 2) + 21], 16);
                dataNFC[i] = Convert.ToInt32(valSplit[(i * 2) + 20] + valSplit[(i * 2) + 21], 16);
                result += " " + intAry[i + 20].ToString("D5");
            }
            Debug.Log("convert to int all data  " + result);
            string bodyDisplayStr = "";
            string idCard = "";
            for (int i = 0; i < 8; i++)
            {
                idCard += intAry[i].ToString();
            }
            bodyDisplayStr = "ID: "+idCard + "\r\n";
            bodyDisplayStr += "Time Start: " + intAry[8].ToString("D2") + "-" + intAry[9].ToString("D2") +"-" +"20" +intAry[10].ToString("D2") + " " + intAry[11].ToString("D2") + ":" + intAry[12].ToString("D2") + ":" + intAry[13].ToString("D2") + "\r\n";
            bodyDisplayStr += "Time End: " + intAry[14].ToString("D2") + "-" + intAry[15].ToString("D2") + "-" + "20" + intAry[16].ToString("D2") + " " + intAry[17].ToString("D2") + ":" + intAry[18].ToString("D2") + ":" + intAry[19].ToString("D2") + "\r\n";
            for (int i = 0; i < lenData; i++)
            {
                string NFCBuf=dataNFC[i].ToString("D5");
                for(int y=0;y<NFCBuf.Length;y++)
                {
                    char NFCChar = NFCBuf[y];
                    if(NFCChar!='0')
                    {
                        int boothIndex = ((i * 5) + y + 1);
                        bodyDisplayStr += "booth: " + boothIndex + " point: " + NFCChar.ToString() + "\r\n";
                        boothList.Add(boothIndex);
                        //Debug.Log("booth: " + boothIndex + " point: " + NFCChar.ToString());
                    }
                }
            }
            bodyTx.text = bodyDisplayStr;
            float bodyHeight = bodyTx.GetComponent<Text>().preferredHeight;
            contentScrollView.GetComponent<RectTransform>().offsetMin = new Vector2(0f, -(bodyHeight + 70f));
            for(int i =0;i<boothList.Count;i++)
            {
                if(boothList[i].ToString()==boothNOInput.text)
                {
                    // duplicatie
                    toastString = "already score this booth";
                    currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
                    return;
                }
            }
            if(boothNOInput.text == "50")
            {
                headerBoothScoreTx.text = "สหพัฒน์แฟร์ 2016\nกรุณาให้คะแนนภาพรวมของงาน\nPlease score for this fair";
                Debug.Log("50 booth");
            }
            else
            {
                headerBoothScoreTx.text = "สหพัฒน์แฟร์ 2016\nกรุณาให้คะแนน บูธ\nPlease score for booth";
            }
            ScorePopup.SetActive(true);
        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
        }
    }
    #endregion
}
